variable "gitlab_token" {
    type = string
    description = "Personal gitlab access token"
}

variable "base_gitlab_url" {
    type = string
    description = "Gitlab APU URL"
}

variable "ssh_public_key" {
    type = string
    description = "Personal public SSH key"
}

variable "gitlab_group" {
    type = number
    description = "Gitlab group"
}
