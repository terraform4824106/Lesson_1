terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "15.8.0"
    }
  }
}

provider "gitlab" {
    token = var.gitlab_token
    base_url = var.base_gitlab_url
}

# On GitLab SaaS, you must use the GitLab UI to create groups without a parent group. You cannot use the API to do this.
# https://docs.gitlab.com/ee/api/groups.html#new-group

# resource "gitlab_group" "Terraform" {
#   name        = "Terraform"
#   path        = "terraform_eduard"
#   description = "Group for terrafrom practice"
# }

resource "gitlab_project" "TF_Lesson_1" {
    name = "Lesson_1"
    namespace_id = var.gitlab_group
    visibility_level = "public"
}

resource "gitlab_deploy_key" "eduard_deploy_key" {
  project = gitlab_project.TF_Lesson_1.id
  title   = "Eduard's deploy key"
  key     = var.ssh_public_key
  can_push = true
}